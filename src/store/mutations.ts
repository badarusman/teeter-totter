import { MutationTree } from "vuex";
import { RootState } from "./types";
import {
  TOGGLE_PAUSE,
  ADD_RIGHT_SIDE_OBJECT,
  ADD_LEFT_SIDE_OBJECT,
  RESET_STATE,
  MAX_BENDING,
  MIN_BENDING,
  MAX_SIDES_DIFFERENCE,
  FALLING_OBJECTS_COUNT,
  FINISH_FALLING,
  START_NEW_GAME,
  INITIALIZE_FALLING_OBJECTS,
  ADD_FALLING_OBJECT,
  MOVE_RIGHT,
  MOVE_LEFT
} from "../utilities";

// utility function
import { v4 } from "uuid";
import {
  MIN_WEIGHT,
  MAX_WEIGHT,
  TEETER_TOTTER_WIDTH,
  SHAPE_COUNT,
  SHAPES
} from "../utilities";

export function generateRandomObject() {
  const id = v4();
  const type = SHAPES[Math.floor(Math.random() * SHAPE_COUNT)];
  const weight = Math.floor(Math.random() * MAX_WEIGHT) + MIN_WEIGHT;
  const offset = Math.floor((Math.random() * TEETER_TOTTER_WIDTH) / 2) + 1;
  const height = weight * 8;

  return {
    id,
    type,
    weight,
    offset,
    height
  };
}

export const mutations: MutationTree<RootState> = {
  togglePause(state: RootState) {
    state.isStopped = !state.isStopped;
  },

  addRightSideObject(state: RootState) {
    const randomObject = generateRandomObject();
    state.rightSideObjects.push(randomObject);
  },

  addLeftSideObject(state: RootState) {
    const randomObject = generateRandomObject();
    state.leftSideObjects.push(randomObject);
  },

  addFallingObject(state: RootState) {
    const randomObject = generateRandomObject();
    state.fallingObjects.push(randomObject);
  },

  moveRight(state: RootState) {
    if (state.isStopped || state.fallingObjects[0].offset - 1 <= 0) return;
    state.fallingObjects[0].offset -= 1;
  },

  moveLeft(state: RootState) {
    if (state.isStopped || state.fallingObjects[0].offset + 1 > 5) return;
    state.fallingObjects[0].offset += 1;
  },

  initializeFallingObjects(state: RootState) {
    state.fallingObjects = [];
    for (let i = 0; i < FALLING_OBJECTS_COUNT; i++) {
      const randomBlock = generateRandomObject();
      state.fallingObjects.push(randomBlock);
    }
  },

  resetState(state: RootState) {
    state.isStopped = true;
    state.leftSideObjects = [];
    state.rightSideObjects = [];
    state.fallingObjects = [];
  }
};
