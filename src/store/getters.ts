import { GetterTree } from "vuex";
import { RootState } from "./types";
import { MAX_BENDING, MIN_BENDING, MAX_SIDES_DIFFERENCE } from "../utilities";

// utitlity Funtion
function getBlockPower(array: any) {
  return array.reduce((acc: any, item: any) => {
    return (acc += item.weight * item.offset);
  }, 0);
}
export const getters: GetterTree<RootState, any> = {
  isStopped: state => state.isStopped,
  leftSum: state => getBlockPower(state.leftSideObjects),
  rightSum: state => getBlockPower(state.rightSideObjects),
  beamBending: (state, getters) => {
    const { leftSum, rightSum } = getters;
    if (!leftSum) return MAX_BENDING;
    if (leftSum === rightSum) return 0;
    return leftSum > rightSum
      ? ((leftSum - rightSum) / leftSum) * -100
      : ((rightSum - leftSum) / rightSum) * 100;
  },

  gameOverStatus: (state, getters) => {
    const { leftSum, rightSum, beamBending } = getters;
    if (!leftSum) return MAX_BENDING;
    if (leftSum === rightSum) return 0;
    return (
      beamBending > MAX_BENDING ||
      beamBending < MIN_BENDING ||
      Math.abs(leftSum - rightSum) > MAX_SIDES_DIFFERENCE
    );
  },
  fallingObjects: state => state.fallingObjects,
  leftSideObjects: state => state.leftSideObjects,
  rightSideObjects: state => state.rightSideObjects
};
