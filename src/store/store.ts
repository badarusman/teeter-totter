import Vue from "vue";
import Vuex from "vuex";
import { RootState } from "@/store/types";
import { getters } from "@/store/getters";
import { actions } from "@/store/actions";
import { mutations } from "@/store/mutations";

Vue.use(Vuex);

export const store = new Vuex.Store<RootState>({
  state: new RootState(),
  getters,
  mutations,
  actions
});
