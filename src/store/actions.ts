import Vue from "vue";
import { ActionTree } from "vuex";
import { RootState } from "./types";

export const actions: ActionTree<RootState, any> = {
  finishFalling({ commit, state, getters, dispatch }) {
    commit("addLeftSideObject");
    commit("addFallingObject");

    if (
      state.leftSideObjects.length &&
      state.leftSideObjects.length !== state.rightSideObjects.length
    )
      commit("addRightSideObject");
    if (getters.gameOverStatus)
      setTimeout(() => {
        alert("game over");
        dispatch("startNewGame");
      }, 0);
  },

  startNewGame({ commit }) {
    commit("resetState");
    commit("addRightSideObject");
    commit("initializeFallingObjects");
  },

  initializeFallingObjects({ commit }) {
    commit("initializeFallingObjects");
  },

  fallingObjects({ commit }) {
    commit("fallingObjects");
  },

  addRightSideObject({ commit }) {
    commit("addRightSideObject");
  },

  moveObjectRight({ commit }) {
    commit("moveRight");
  },

  moveObjectLeft({ commit }) {
    commit("moveLeft");
  },

  pauseToggle({ commit }) {
    commit("togglePause");
  }
};
