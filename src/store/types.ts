export class RootState {
  isStopped: boolean = true;
  leftSideObjects: any = [];
  rightSideObjects: any = [];
  fallingObjects: any = [];
}
