// Constant Values
export const TEETER_TOTTER_WIDTH = 10;
export const SHAPE_COUNT = 3;
export const SHAPES = ["circle", "rectangle", "triangle"];
export const MAX_WEIGHT = 10;
export const MIN_WEIGHT = 1;
export const MAX_BENDING = 30;
export const MIN_BENDING = -30;
export const MAX_SIDES_DIFFERENCE = 20;
export const FALLING_OBJECTS_COUNT = 2;
export const INITIAL_TIMEOUT = 300;
export const TIMEOUT_STEP_DECREASING = 50;
export const ITERATION_COUNT_INCREASING = 5;

export const TOGGLE_PAUSE = "TOGGLE_PAUSE";
export const ADD_RIGHT_SIDE_OBJECT = "ADD_RIGHT_SIDE_OBJECT";
export const ADD_LEFT_SIDE_OBJECT = "ADD_LEFT_SIDE_OBJECT";
export const RESET_STATE = "RESET_STATE";
export const FINISH_FALLING = "FINISH_FALLING";
export const START_NEW_GAME = "START_NEW_GAME";
export const INITIALIZE_FALLING_OBJECTS = "INITIALIZE_FALLING_OBJECTS";
export const ADD_FALLING_OBJECT = "ADD_FALLING_OBJECT";
export const MOVE_RIGHT = "MOVE_RIGHT";
export const MOVE_LEFT = "MOVE_LEFT";
